#ifndef __cuadrados_h_
#define __cuadrados_h_

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <jsoncpp/json/value.h>

#include "shader.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <chrono> //timing

/**
 * EXTERN VARIABLES DECLARATION
 */
extern Json::Value json_parameters;

/**
 * Global values
 */
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
std::vector<std::string> split(std::string str, std::string token);
GLFWwindow* initializeOpenGL();
float distance(glm::vec2 v1, glm::vec2 v2);
bool collision(glm::vec2 v1, glm::vec2 v2, int nSquares, int i);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

class Cuadrados
{
public:
    void run();
};
#endif