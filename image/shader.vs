#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 2) in vec2 aOffset;
layout (location = 3) in vec3 aColor;

out vec3 fColor;

void main()
{
    fColor = aColor;

    gl_Position = vec4(aPos.x + aOffset.x, 
                    aPos.y + aOffset.y, 
                    0.0, 1.0);
}