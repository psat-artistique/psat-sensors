#include <iostream>
#include <fstream>
#include <algorithm>
#include <thread>

#include "zmq.hpp"
#include "cuadrados.h"

#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/value.h>

using namespace std;


/**
* Server connexion
*/
zmq::context_t ctx(1);
//zmq::socket_t subscriber(ctx, zmq::socket_type::pull);

/**
 * EXTERN VARIABLES DEFINITION
 */
Json::Value json_parameters(Json::objectValue);

/**
 * Read data & update picture
 */
void pullClient() {
    zmq::socket_t subscriber(ctx, zmq::socket_type::pull);
    subscriber.connect("tcp://localhost:5555");
    zmq::message_t update;
    for (;;)
    {
        subscriber.recv(update);
        string message = string (static_cast<char *>(update.data()), update.size());
        /**
         * Delete quotes and backslash; find another method if possible
         */
        string jsonMsg = message.substr(1, message.size() - 2);
        jsonMsg.erase(remove(jsonMsg.begin(), jsonMsg.end(), '\\'), jsonMsg.end());

        // Json::Reader reader;
        Json::CharReaderBuilder builder;
        Json::CharReader * reader = builder.newCharReader();
        string errors;
        bool parsingSuccessful = reader->parse(jsonMsg.c_str(), jsonMsg.c_str() + jsonMsg.size(), &json_parameters, &errors);
        delete reader;
        //bool parsingSuccessful= reader.parse(jsonMsg,json_parameters);
        if ( !parsingSuccessful )
        {
            // cout << "error : " << reader.getFormattedErrorMessages() << endl;
            cout << "error : " << errors << endl;
        }

    }
}

void displayPicture() {
    /**
     * Picture object
     */
    Cuadrados cuad;
    cuad.run();
}

int main()
{

    /**
     * CREATE THREADS
     */
    thread server_thread(pullClient);
    thread picture_thread(displayPicture);

    picture_thread.join();
    server_thread.join();

    return 0;
}   