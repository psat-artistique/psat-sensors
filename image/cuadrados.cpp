#include "cuadrados.h"

void Cuadrados::run()
{
   
    GLFWwindow* window = initializeOpenGL();

    // build and compile shaders
    Shader shader("shader.vs", "shader.fs");

    const int nSquares = 625; 
    glm::vec2 translations[nSquares], speeds[nSquares];
    glm::vec3 colors[nSquares], colorSpeeds[nSquares];
    int index = 0;

    // starting positions, speeds, directions and colors for the squares 
    for (float y = -9; y < 10; y += 20 / sqrt(nSquares))
    {
        for (float x = -9; x < 10; x += 20 / sqrt(nSquares))
        {
            glm::vec2 translation;
            glm::vec3 color, speed;

            translation.x = x * 0.1;
            translation.y = y * 0.1;
            translations[index] = translation;

            speed.x = 0.0;
            speed.y = 0.0; 

            color.r = 0.3;
            color.g = 0.0;
            color.b = 0.5;

            colorSpeeds[index] = glm::vec3(0.0, 0.0, 0.0);
            speeds[index] = speed;
            colors[index] = color;

            index++;
        }
    }

    // store instance data in an array buffer
    unsigned int translationsVBO;
    glGenBuffers(1, &translationsVBO);
    glBindBuffer(GL_ARRAY_BUFFER, translationsVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * nSquares, &translations[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    unsigned int colorsVBO;
    glGenBuffers(1, &colorsVBO);
    glBindBuffer(GL_ARRAY_BUFFER, colorsVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * nSquares, &colors[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // set up vertex data (and buffer(s)) and configure vertex attributes
    float quadVertices[] = {
        // positions  
        -0.03f,  0.03f, // top left 
         0.03f, -0.03f, // bottom right
        -0.03f, -0.03f, // bottom left

        -0.03f,  0.03f, // top left
         0.03f, -0.03f, // bottom right 
         0.03f,  0.03f, // top right
    };

    unsigned int quadVAO, quadVBO;
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);

    // also set instance data
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, translationsVBO); 
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glVertexAttribDivisor(2, 1); 

    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, colorsVBO);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glVertexAttribDivisor(3, 1);


    glm::vec2 target;
    target.x = 0.0;
    target.y = 0.0;

    int iteration = 0;

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    // number of lines in file to process and average at a time
    int iter_lines = 10;
    float read_speed = (1/100.0 * (float) iter_lines)*1000000; //more or less 100 lines per second in microseconds to match timer

    std::fstream data;
    data.open("data/bras3-2capteurs.csv", std::ios::in);
    std::string tp;
    std::getline(data, tp);
    std::chrono::steady_clock::time_point last_read = std::chrono::steady_clock::now();

    float prevx = 0; 
    // render loop
    while (!glfwWindowShouldClose(window))
    {
        processInput(window);

        // render
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader.use();
        glm::vec2 newPos;

        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        float time = (float) std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()/10000;
        begin = end; 

        float time2 = (float)std::chrono::duration_cast<std::chrono::microseconds>(end - last_read).count();
        
        glm::vec3 acc, gyr;
        acc = glm::vec3(0.0, 0.0, 0.0);
        gyr = glm::vec3(0.0, 0.0, 0.0);

        if(!json_parameters.empty())
        {
            //TODO there is a problem when I use this for the source id 
            //auto id = json_parameters.getMemberNames()[0];
            auto id = "54.0";

            acc.x = json_parameters[id]["accX_median"].asFloat();
            acc.y = json_parameters[id]["accY_median"].asFloat();
            acc.z = json_parameters[id]["accZ_median"].asFloat();

            gyr.x = json_parameters[id]["gyrX_median"].asFloat();
            gyr.y = json_parameters[id]["gyrY_median"].asFloat();
            gyr.z = json_parameters[id]["gyrZ_median"].asFloat();
        }

        // update speed and color of squares 
        
        for (int i = 0; i < nSquares; i++) {

            speeds[i].x +=  acc.x * time / 200000; 
            speeds[i].y +=  acc.y * time / 200000;

            colorSpeeds[i] += gyr * (time / 200000);

            if (prevx != acc.x) {
                std::cout << gyr.x << " " << gyr.y << " " << gyr.z << std::endl;
                prevx = acc.x;
            }         
            
        }

        // update translations according to speed of squares
        for (int i=0; i < nSquares; i++) {

            translations[i].x = translations[i].x + speeds[i].x * time;
            translations[i].y = translations[i].y + speeds[i].y * time;

            colors[i] = colors[i] + colorSpeeds[i] * time;

            //check that it doesnt go out of range

            if (translations[i].x >= 1) {
                translations[i].x = 1;
                speeds[i].x *= -1;
            }
            else if (translations[i].x <= -1) {
                translations[i].x = -1;
                speeds[i].x *= -1;
            }

            if (translations[i].y >= 1) {
                translations[i].y = 1;
                speeds[i].y *= -1;
            }
            else if (translations[i].y <= -1) {
                translations[i].y = -1;
                speeds[i].y *= -1;
            }

        } 

        glBindBuffer(GL_ARRAY_BUFFER, translationsVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * nSquares, &translations[0], GL_DYNAMIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, colorsVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)* nSquares, &colors[0], GL_DYNAMIC_DRAW);

        glBindVertexArray(quadVAO);
        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, nSquares);
        glBindVertexArray(0);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        glfwSwapBuffers(window);
        glfwPollEvents();
        iteration++;

    }

    // optional: de-allocate all resources once they've outlived their purpose:
    glDeleteVertexArrays(1, &quadVAO);
    glDeleteBuffers(1, &quadVBO);

    glfwTerminate();
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

std::vector<std::string> split(std::string str, std::string token) {
    std::vector<std::string>result;
    while (str.size()) {
        int index = str.find(token);
        if (index != std::string::npos) {
            result.push_back(str.substr(0, index));
            str = str.substr(index + token.size());
            if (str.size() == 0)result.push_back(str);
        }
        else {
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

GLFWwindow* initializeOpenGL() {
    // glfw: initialize and configure
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // glfw window creation
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
    }
    glfwMakeContextCurrent(window);

    // glad: load all OpenGL function pointers
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
    }

    // configure global opengl state
    glEnable(GL_DEPTH_TEST);

    return window;
}

float distance(glm::vec2 v1, glm::vec2 v2) {
    return sqrt(pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2));
}


bool collision(glm::vec2 v1, glm::vec2 v2, int nSquares, int i) {

    bool collision = false;
    for (int j = 0; j < nSquares; j++) {

        if (i != j) {
            bool collisionX = v1.x + 0.03f >= v2.x &&
               v2.x + 0.03f >= v1.x;

            bool collisionY = v1.y + 0.03f >= v2.y &&
                v2.y + 0.03f >= v1.y;

            // collision only if on both axes
            collision = collisionX && collisionY;

            if (collision) {
                return collision;
            }
        }

    }
    return collision;

}