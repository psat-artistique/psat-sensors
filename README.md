This project is used to get data from Hikob accelerometers, communicating them to a C++/OpenGL client, and making an image evolve.

This branch contains : 
* python_server : the python server 1) acquires data from the serial Hikob gateway 2) formats the data 3) sends the data to the cpp client
* cpp_client : a generic cpp client for receiving the data and writing it to a file

Lien de téléchargement des sources EvoMove : https://filesender.renater.fr/?s=download&token=0d887c78-1fc8-4758-b8e0-1baf8ec0a76c (valable jusqu'au 21/11)


=======
# How is the data acquired ? 

The accelerometer system consists of 4 sensors and 1 gateway. The gateway is connected to the server machine via a serial usb port. The python server uses `pyserial` to collect the data. 

# How is the data passed between server and client ? 

The communication is implemented using the `0mq` library, using a PUSH/PULL protocol on TCP. 
Data is passed as JSON objects.

# Requirements and configuration 

* All python requirements can be installed via pip : 
    ```pip install -r python_server/requirements.txt```

* See `cpp_client/README.md` for the installation of C++ `0mq` libraries.

* A `conf.yaml` file must be included in `python_server/` to specify the port used for serial communication. 
   
      `port : # your port here`


# Compilation

The cpp client is compiled via cmake : 
    
    
    cd cpp_client
    cmake .
    make
    

# Running the project

* Connect the gateway to your serial port 
* Turn on the gateway, then the sensors
* Launch server `python python_server/sensorServer.py start` (you may need sudo)
* Launch the client ̀`cpp_client/client`
* Data is written to ̀`cpp_client/output.json`

/!\ everytime the client receives data, a new json array is written to output.json, making output.json an INVALID json file, do not read it for computations. 


## IMAGE

### Set up du projet

#### Windows 

<div class="panel-body">

Il est nécessaire d'avoir installé le <b> Développement Desktop en C++ </b> et le composant <b> Runtime C windows universel </b> sur votre version de Visual Studio pour éviter des erreurs dans ce projet.
Si nécessaire, les ajouter dans le <b>Visual Studio Installer </b>

</div>


- Ouvrir la solution dans "psat-sensors\image\learnopengl.sln
- Faire une clic droit sur le projet <b>learnopengl </b> -> <b>Propriétés</b> dans l'explorateur de solutions
- Aller dans <b>Propriétés de configuration</b> -> <b>Répertoires VC++</b> pour ajouter les includes et les librairies
- Dans <b>répertoire d'include</b> ajouter le dossier <b>glfw-3.3.2.bin.WIN64\include</b> (ou WIN32 si Windows 32 bits au lieu de 64)
- Dans <b>répertoire de bibliothèques</b> ajouter le dossier <b>glfw-3.3.2.bin.WIN64\lib-vc20XX</b> (où XX correspond à la version de Visual Studio)
![Alt text](Readme_img/lib.JPG?raw=true "Image1") 
- Toujours dans <b>propriétés -> général </b> vérifier que <b>Ensemble d'outils de plateformes</b> correspond bien à votre version de Visual Studio
![Alt text](Readme_img/plateforme.JPG?raw=true "Image2") 
- Aller dans <b>Projet</b> -> <b>Recibler la solution </b> et choisir la version SDK de windows la plus récente.
- Générer la solution
- Exécuter le code et si tout se passe bien la fenêtre apparaitra.

#### Linux

 - Prérequis : les paquets *glfw3, OpenGL, jsoncpp, glad* doivent être  installés
    - jsoncpp : `sudo apt install libjsoncpp-dev`
    - dépendances nécessaires pour la suite : *déso là j'ai pas la liste précise je peux donner des idées si y'a des problèmes : build-essential libgl1-mesa-dev libglew-dev libsdl2-dev libsdl2-image-dev libglm-dev libfreetype6-dev libglu1-mesa-dev freeglut3-dev mesa-common-dev*
    - glad : https://glad.dav1d.de/ &rarr; télécharger la version de Glad avec une version de gl supérieure à 3.3, "Profile" = Core
    - glfw3 : `git clone https://github.com/glfw/glfw.git | cd glfw | cmake . | make | sudo make install`
 - Placer le fichier `glad.c` dans le dossier image/
 - Placer dans les includes (/usr/local/include/) les dossiers contenant les headers de glad (glad/ et KHR)
 - Dans un terminal, se rendre dans le dossier image/
 - Taper la commande `cmake . | make | ./psat`
