#include <iostream>
#include "zmq.hpp"
#include <fstream>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/value.h>

int main()
{
    zmq::context_t ctx(1);

    zmq::socket_t subscriber(ctx, zmq::socket_type::pull);
    // subscriber.connect("ipc:///tmp/sensors1");
    subscriber.connect("tcp://localhost:5555");
    // zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "", 0);

    // std::ofstream myfile("out.json");

    zmq::message_t update;
    for (;;)
    {
        subscriber.recv(&update);
        std::string message = std::string(static_cast<char *>(update.data()), update.size());
        // myfile << message << std::endl; 
        Json::Reader rdr;
        Json::Value jsonValue;
        bool parsingSuccessful = rdr.parse(message.c_str(), jsonValue); //parse process
        if (!parsingSuccessful)
        {
            std::cout << "Failed to parse" << rdr.getFormattedErrorMessages();
        }
        std::cout << jsonValue <<std::endl;
        // std::cout << jsonValue.get("49.0", "No sensors").asString() << std::endl;
    }
}