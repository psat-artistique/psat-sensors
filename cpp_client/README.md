# Requirements

To run this program you need to : 
* install package libzmq, see : https://github.com/zeromq/libzmq
(Do not follow cppmzq instructions for the libzmq build, but rather libzmq's directly)
* install package cppzmq :
    * download zip from https://github.com/zeromq/cppzmq
    * unzip, cd to directory
    * mkdir build
    * cd build
    * cmake ..
    * sudo make -j4 install

# Build

Run 
```
cmake .
make
```

# Run 
`./client` 
You may need to use sudo privileges.
