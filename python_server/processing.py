import pandas


def check_sign(df):
    cumulative_bool = {"x_pos": False, "x_neg": False, "y_pos": False, "y_neg": False, "z_pos": False, "z_neg": False}
    x_pos, x_neg, y_pos, y_neg, z_pos, z_neg = 0,0,0,0,0,0
    for row in df.iterrows():
        if not cumulative_bool["x_pos"] and row[1]["magX"] >= 0 :
            x_pos += 1
            x_neg = 0
            if x_pos == 5 :
                cumulative_bool["x_pos"] = True 
        if not cumulative_bool["x_neg"] and row[1]["magX"] < 0 :
            x_neg += 1
            x_pos = 0
            if x_neg == 5 :
                cumulative_bool["x_neg"] = True 
        if not cumulative_bool["y_pos"] and row[1]["magY"] >= 0 :
            y_pos += 1
            y_neg = 0
            if y_pos == 5 :
                cumulative_bool["y_pos"] = True 
        if not cumulative_bool["y_neg"] and row[1]["magY"] < 0 :
            y_neg += 1
            y_pos = 0
            if y_neg == 5 :
                cumulative_bool["y_neg"] = True 
        if not cumulative_bool["z_pos"] and row[1]["magZ"] >= 0 :
            z_pos += 1
            z_neg = 0
            if z_pos == 5 :
                cumulative_bool["z_pos"] = True 
        if not cumulative_bool["z_neg"] and row[1]["magZ"] < 0 :
            z_neg += 1
            z_pos = 0
            if z_neg == 5 :
                cumulative_bool["z_neg"] = True 
    return {"x" : cumulative_bool["x_pos"] and cumulative_bool["x_neg"], 
            "y" : cumulative_bool["y_pos"] and cumulative_bool["y_neg"], 
            "z" : cumulative_bool["z_pos"] and cumulative_bool["z_neg"]}

def process_data(df):

    """
      df  sensorNumber : {
            "accX_median": 0.0,
            "accY_median": 0.0,
            "accZ_median": 0.0,
            "magX":0/1,
            "magY": 0/1,
            "magZ": 0/1,
            "gyrX_median": 0.0,
            "gyrY_median": 0.0,
            "gyrZ_median": 0.0
        }
    """
    df['src']= df["src"].astype(int)
    result_df = df.groupby('src').agg(
        {
            'accX':['median'],
            'accY':['median'],
            'accZ':['median'],
            'gyrX':['median'],
            'gyrY':['median'],
            'gyrZ':['median'],
        })
    result_df.columns = ["_".join(x) for x in result_df.columns.ravel()]
    for src in list(set(df['src'])):
        check_sign_result = check_sign(df[df["src"] == src])
        result_df.at[src, "magX"] = check_sign_result["x"]
        result_df.at[src, "magY"] = check_sign_result["y"]
        result_df.at[src, "magZ"] = check_sign_result["z"]
    return result_df