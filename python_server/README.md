# Requirements

All required packages can be installed through
`pip install -r requirements.txt`

# Configuration

You must have a conf file `conf.yaml` containing a port field : 
```
port : #your port here
```

# Running

You can launch the program in CLI through
`python sensorServer.py start`

To get help
`python sensorServer --help`

You may need to use sudo. 

# serial_to_csv.py

This is a py module used to test the sensors or collect data in files.
`python serial_to_csv.py`

You may need to use sudo.