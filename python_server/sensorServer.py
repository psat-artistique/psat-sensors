import serial
import struct
import click
import sys
import yaml
from threading import Thread, RLock
import zmq
import time
import pandas as pd

from processing import process_data

## initialisation

# create buffer
fieldnames = ['src', 'seq', 'time','accX', 'accY', 'accZ', 'magX', 'magY', 'magZ', 'gyrX', 'gyrY', 'gyrZ']
df_packets= pd.DataFrame(columns=fieldnames)

# locker 
locker = RLock()

# zmq objects
ctx = zmq.Context()
sock = ctx.socket(zmq.PUSH)
sock.bind("tcp://*:5555") 

# message format
HEADER = [0xb0, 0x0b, 0x5b, 0x00, 0xb5]
HEADER_SIZE = 5
PAYLOAD_SIZE = 44

## Class reader_thread: thread which read data from the buffer & compute
class reader_thread(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global df_packets
        print("thread start ok")
        with locker:
            data_send= process_data(df_packets)
            print(data_send)
            sock.send_json(data_send.to_json(orient='index'))
            df_packets= df_packets[0:0]
        print('render lock')

## reading

def processReceivedBytes(bytesArray):
    canContainPayload = (len(bytesArray) >= (2*HEADER_SIZE+PAYLOAD_SIZE))

    while canContainPayload :
        canContainPayload = False
        # Find first header in bytesArray
        header1 = None
        for i in range(0,len(bytesArray)):
            if [chr(i) for i in bytesArray[i:i+HEADER_SIZE]] == [chr(i) for i in HEADER]:
                header1 = i
                break
        # If a header has been found at pos header1
        if not header1 is None :
            header2 = None
            # Find a second header
            for i in range(header1+HEADER_SIZE,len(bytesArray)):
                if [chr(i) for i in bytesArray[i:i+HEADER_SIZE]] == [chr(i) for i in HEADER]:
                    header2 = i
                    break
            # If a second header has been found at pos header2
            if not header2 is None :
                payload = bytesArray[header1+HEADER_SIZE:header2]
                if len(payload) == PAYLOAD_SIZE:
                    processPayload(payload)
                bytesArray = bytesArray[header2:]
                canContainPayload = (len(bytesArray) >= (2*HEADER_SIZE+PAYLOAD_SIZE))

def processPayload(payload):
    global df_packets
    data = struct.unpack('<HHffffffffff', payload)
    entry = {}
    for i in range(0, len(fieldnames)):
        entry[fieldnames[i]] = data[i]
    with locker:
        df_packets= df_packets.append(entry, ignore_index=True)

@click.group()
def main():
    """A program to read data from Hikob accelerometers"""
    pass

@main.command()
def start():
    with open("conf.yaml", "r") as ymlfile:
        cfg = yaml.load(ymlfile)
    port = cfg["port"]

    # open serial port
    try:
        ser = serial.Serial(port, baudrate=230400, timeout=1)
    except Exception as err :
        print('Could not open port ', port)
        print(err)
        sys.exit(1)
    print('Reading on port ', port)

    start= time.time()

    while(True):
        bytesArray = ser.readline()
        processReceivedBytes(bytesArray)
        check= time.time()
        # sending each second
        if(check - start >= 1):
            start = check
            ## creating thread & start
            reader_th = reader_thread()
            reader_th.start()
    sock.close()
    ctx.term()

if __name__ == '__main__':
    args = sys.argv
    if "--help" in args or len(args) == 1:
        print("SensorServer")
    main()
