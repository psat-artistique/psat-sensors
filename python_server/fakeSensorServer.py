import serial
import struct
import click
import sys
import yaml
from threading import Thread, RLock
import zmq
import time
import pandas as pd


## initialisation

# create buffer
fieldnames = ['src', 'seq', 'time','accX', 'accY', 'accZ', 'magX', 'magY', 'magZ', 'gyrX', 'gyrY', 'gyrZ']
df_packets= pd.DataFrame(columns=fieldnames)

# locker 
locker = RLock()

# zmq objects
ctx = zmq.Context()
sock = ctx.socket(zmq.PUSH)
sock.bind("tcp://*:5555") 

# message format
HEADER = [0xb0, 0x0b, 0x5b, 0x00, 0xb5]
HEADER_SIZE = 5
PAYLOAD_SIZE = 44

## Class reader_thread: thread witch read data from the buffer & compute
class reader_thread(Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global df_packets
        with locker:
            data_send= aggregate_data()
            sock.send_json(data_send.to_json(orient='index'))
            df_packets= df_packets[0:0]
 

## Aggregation
def aggregate_data():
    df_agregate = df_packets.groupby('src').agg(
        {
            'accX':['median'],
            'accY':['median'],
            'accZ':['median'],
            'magX':['median'],
            'magY':['median'],
            'magZ':['median'],
            'gyrX':['median'],
            'gyrY':['median'],
            'gyrZ':['median'],
        })
    df_agregate.columns = ["_".join(x) for x in df_agregate.columns.ravel()]
    return df_agregate


@click.group()
def main():
    """A program to read data from Hikob accelerometers"""
    pass

@main.command()
def start():
    global df_packets
    global fieldnames
    fakeData = open("data/bras2.csv", "r")

    n = 0 
    s = 0 
    fakeData.readline()
    while(True):
        
        line = fakeData.readline()    
        
        if not line:
            fakeData.seek(0)
            fakeData.readline()
            line = fakeData.readline()
            print("re starting...")

        line = [float(i) for i in line.strip().split(",")]
        
        df_packets= df_packets.append(pd.Series(line, index=fieldnames), ignore_index=True)
        n += 1

        if(n == 50):
            ## creating thread & start
            time.sleep(1)
            print(s)
            s += 1
            n = 0
            reader_th = reader_thread()
            reader_th.start()
            
    sock.close()
    ctx.term()

if __name__ == '__main__':
    args = sys.argv
    if "--help" in args or len(args) == 1:
        print("SensorServer")
    main()
