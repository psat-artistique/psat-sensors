import serial
import struct
import csv

packets = []
fieldnames = ['src', 'seq', 'time','accX', 'accY', 'accZ', 'magX', 'magY', 'magZ', 'gyrX', 'gyrY', 'gyrZ']

with open('data.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

def processMessage(message):
    unp = struct.unpack('<HHffffffffff', message)
    writeJSON(unp)

def writeJSON(data):
    with open('data.csv', 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'src': data[0],
                            'seq': data[1],
                            'time': data[2],
                            'accX': data[3],
                            'accY': data[4],
                            'accZ': data[5],
                            'magX': data[6],
                            'magY': data[7],
                            'magZ': data[8],
                            'gyrX': data[9],
                            'gyrY': data[10],
                            'gyrZ': data[11]})




#READING FROM SERIAL PORT
ser = serial.Serial("/dev/ttyACM1", baudrate=230400)
print('Reading ', ser.name)

line = ser.readline()

print('Read', len(line))

# LINE FORMAT
HEADER = [0xb0, 0x0b, 0x5b, 0x00, 0xb5]
HEADER_SIZE = 5
PAYLOAD_SIZE = 44

# PROCESSING RECEIVED BYTES
canContainMessage = (len(line) >= (2*HEADER_SIZE+PAYLOAD_SIZE))

while canContainMessage :
    canContainMessage = False
    # Find first header in line
    header1 = None
    for i in range(0,len(line)):
        if [chr(i) for i in line[i:i+HEADER_SIZE]] == [chr(i) for i in HEADER]:
            header1 = i
            break
    # If a header has been found at pos header1
    if not header1 is None :
        header2 = None
        # Find a second header
        for i in range(header1+HEADER_SIZE,len(line)):
            if [chr(i) for i in line[i:i+HEADER_SIZE]] == [chr(i) for i in HEADER]:
                header2 = i
                break
        # If a second header has been found at pos header2
        if not header2 is None :
            message = line[header1+HEADER_SIZE:header2]
            if len(message) == PAYLOAD_SIZE:
                processMessage(message)
            line = line[header2:]
            canContainMessage = (len(line) >= (2*HEADER_SIZE+PAYLOAD_SIZE)) 